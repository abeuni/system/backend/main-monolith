module.exports = {
  type: 'postgres',
  url: process.env.PGSQL_URI || '',
  synchronize: true,
  logging: false,
  entities: [
    'dist/**/*.js'
  ],
  migrations: [
    'src/migrations/**/*.ts'
  ],
  subscribers: [
    'src/subscribers/**/*.ts'
  ]
}
