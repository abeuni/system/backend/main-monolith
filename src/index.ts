import 'reflect-metadata'

import './orm'

import express from 'express'
import { Server, Errors } from 'typescript-rest'

import { logger } from './logging'
import { middlewares } from './schema'

import cors from 'cors'
import { HttpError } from 'typescript-rest/dist/server/model/errors'
import { QueryFailedError } from 'typeorm'

// Configurations
const port = process.env.PORT || 3000

// Create App and Start listening
const app = express()

// CORS Setup
app.use(cors())

// Load Middlewares
middlewares.forEach((middleware) => {
  app.use(middleware)
})

Server.buildServices(app)

// Error handling middleware
app.use((err: Error, _req: express.Request, _res: express.Response, next: express.NextFunction) => {
  if (err instanceof QueryFailedError) {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const error = err as any

    // null value in column violates not-null constraint
    if (error.code === '23502') {
      throw new Errors.BadRequestError('Invalid null value')
    }

    // duplicate key value violates unique constraint
    if (error.code === '23505') {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      throw new Errors.ConflictError(`Entry with '${(error as any).detail}'`)
    }

    // invalid input syntax for type uuid
    if (error.code === '22P02') {
      throw new Errors.BadRequestError(`Invalid uuid values: ${error.parameters.join(', ')}`)
    }
  }

  next(err)
})

// Error to status
app.use((err: Error, _req: express.Request, res: express.Response, next: express.NextFunction) => {
  res.set('Content-Type', 'application/json')

  if (err instanceof HttpError) {
    if (res.headersSent) { // important to allow default error handler to close connection if headers already sent
      return next(err)
    }

    res.status(err.statusCode)
    res.json({ error: err.message, code: err.statusCode })
  } else {
    res.status(500)
    res.json({ error: 'Unexpected Server Error', code: 500 })

    console.error(err)

    next(err)
  }
})

// Listen
app.listen(port, () => {
  logger.info(`Listening at ${port}`)
})
