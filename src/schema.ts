import { logger } from './logging'
import { RequestHandler } from 'express'
import { EntitySchema } from 'typeorm'

// Resolvers
export const resolvers = []
const resolverContext = require.context('.', true, /\.resolver\.ts$/)

resolverContext.keys().forEach((key) => {
  logger.info(`Loading resolver: ${key}`)
  resolvers.push(resolverContext(key))
})

// Models
export const models: EntitySchema<unknown>[] = []
const modelContext = require.context('.', true, /\.model\.ts$/)

modelContext.keys().forEach((key) => {
  logger.info(`Loading model: ${key}`)

  models.push(modelContext(key).default)
})

// Middlewares
export const middlewares: RequestHandler[] = []
const middlewareContext = require.context('.', true, /\.middleware\.ts$/)

middlewareContext.keys().forEach((key) => {
  logger.info(`Loading middleware: ${key}`)
  middlewares.push(middlewareContext(key).default)
})
