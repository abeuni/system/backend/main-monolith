import { RequestHandler } from 'express'

import jwt from 'jsonwebtoken'

import { Errors } from 'typescript-rest'

const authMiddleware: RequestHandler = async (req, res, next) => {
  try {
    const authorization = req.header('authorization')

    if (authorization) {
      const [type, token] = authorization.split(' ')

      if (type === 'Bearer' && jwt.verify(token, process.env.JWT_SECRET, {
        algorithms: ['HS256']
      })) {
        const payload = jwt.decode(token) as { [key: string]: string }

        res.locals = {
          user: payload.sub,
          permissions: payload.permissions,
          token: payload
        }
      } else {
        throw new Errors.UnauthorizedError('Authorization header not accepted')
      }
    } else {
      throw new Errors.UnauthorizedError('No authorization header provided')
    }
    next()
  } catch (e) {
    next(e)
  }
}

export default authMiddleware
