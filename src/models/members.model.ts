import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, DeleteDateColumn, BaseEntity, OneToMany } from 'typeorm'
import { IsEmail, IsPhoneNumber, Matches } from 'class-validator'
import MemberGroup from './members_groups_join.model'

export interface ICustomField {
  icon: string,
  name: string,
  value: string
}

export interface ICreateMemberOptions {
  fullname: string,
  email: string,
  RG: string,
  CPF: string,
  birthDate: string,
  phone: string,
  data: { [key: string]: ICustomField }
}

@Entity({ name: 'members' })
export default class Member extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  accid: string;

  @Column()
  fullname: string;

  @IsEmail()
  @Column({ unique: true })
  email: string;

  @Column({ unique: true })
  RG: string;

  @Matches(/^\d\d\d.\d\d\d.\d\d\d-\d\d$/)
  @Column({ unique: true })
  CPF: string;

  @Column('date')
  birthDate: Date;

  @IsPhoneNumber('BR')
  @Column()
  phone: string;

  @OneToMany(() => MemberGroup, mgroup => mgroup.member)
  groups: MemberGroup;

  @Column('json', { nullable: true })
  data: { [key: string]: ICustomField };

  // Basic Timestamps
  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @DeleteDateColumn()
  deleted: Date;
}
