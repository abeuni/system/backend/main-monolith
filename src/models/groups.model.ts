import { Entity, PrimaryGeneratedColumn, Column, OneToMany, CreateDateColumn, UpdateDateColumn, DeleteDateColumn, BaseEntity } from 'typeorm'
import MemberGroup from './members_groups_join.model'
import { Matches, IsOptional, IsString } from 'class-validator'
import { GroupCategory } from './enum.model.util'

export interface ICreateGroupOptions {
  category: string,
  name: string,
  code?: string,
  description?: string,
}

@Entity({ name: 'groups' })
export default class Group extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('enum', { enum: GroupCategory })
  category: string;

  @Column({ unique: true })
  name: string;

  @IsOptional()
  @Matches(/[A-Z]{3,8}/)
  @Column({ unique: true, nullable: true })
  code?: string;

  @IsOptional()
  @IsString()
  @Column('text', { nullable: true })
  description?: string;

  @OneToMany(() => MemberGroup, mgroup => mgroup.group)
  members: MemberGroup;

  // Basic Timestamps
  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @DeleteDateColumn()
  deleted: Date;
}
