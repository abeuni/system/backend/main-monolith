import { Entity, Column, CreateDateColumn, UpdateDateColumn, DeleteDateColumn, ManyToOne, PrimaryColumn, BaseEntity } from 'typeorm'
import Group from './groups.model'
import Member from './members.model'

@Entity({ name: 'members_groups_history' })
export default class MemberGroup extends BaseEntity {
  @PrimaryColumn('uuid')
  @ManyToOne(() => Group, group => group.members)
  group: Group;

  @PrimaryColumn('uuid')
  @ManyToOne(() => Member, member => member.groups)
  member: Member;

  @Column('date')
  startDate: Date;

  @Column('date', { nullable: true })
  endDate: Date;

  // Basic Timestamps
  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @DeleteDateColumn()
  deleted: Date;
}
