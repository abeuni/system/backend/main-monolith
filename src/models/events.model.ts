import { Entity, PrimaryGeneratedColumn, Column, OneToMany, CreateDateColumn, UpdateDateColumn, DeleteDateColumn, ManyToOne, BaseEntity } from 'typeorm'
import EventCategory from './event_categories.model'
import MemberEvent from './members_events_join.model'
import { Matches } from 'class-validator'

export interface ICreateEventOptions {
  category: string,
  name: string,
  code?: string,
  startDate: Date,
  endDate?: Date,
  location?: number[],
  description?: string,
}

@Entity({ name: 'events' })
export default class Event extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => EventCategory, mgroup => mgroup.events)
  category: EventCategory;

  @Column()
  name: string;

  @Matches(/[A-Z0-9]{3,8}/)
  @Column({ nullable: true })
  code: string;

  @Column('date')
  startDate: Date;

  @Column('date', { nullable: true })
  endDate: Date;

  @Column('point', { nullable: true })
  location: number[]

  @Column('text', { nullable: true })
  description: string;

  @OneToMany(() => MemberEvent, mevent => mevent.event)
  members: MemberEvent;

  // Basic Timestamps
  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @DeleteDateColumn()
  deleted: Date;
}
