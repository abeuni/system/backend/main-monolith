import { Path, Context, ServiceContext, Accept, OPTIONS, Errors, POST, GET, PathParam } from 'typescript-rest'
import Event, { ICreateEventOptions } from '@/models/events.model'
import { validate } from 'class-validator'
import { EntityNotFoundError } from 'typeorm/error/EntityNotFoundError'

@Path('events?')
@Accept('application/json')
export class EventsResolver {
  @Context
  context: ServiceContext

  @OPTIONS
  options (): null {
    return null
  }

  @Path('list')
  @GET
  async listEvents (): Promise<Event[]> {
    // Get permissions
    const permissions: string[] = this.context.response.locals.permissions

    // Check if user has permission to create users
    if (!permissions || !permissions.includes('event:list')) {
      throw new Errors.ForbiddenError('You do not have permission to access this resource')
    }

    return Event.find()
  }

  @Path(':id')
  @GET
  async getEvent (@PathParam('id') id: string): Promise<Event> {
    // Get permissions
    const permissions: string[] = this.context.response.locals.permissions

    // Check if user has permission to create users
    if (!permissions || !permissions.includes('event:read')) {
      throw new Errors.ForbiddenError('You do not have permission to access this resource')
    }

    return Event.findOneOrFail({ id }).catch((e) => {
      if (e instanceof EntityNotFoundError) { throw new Errors.NotFoundError(`Event with id ${id} not found`) }
      throw e
    })
  }

  @POST
  async createEvent (data: {
    create: ICreateEventOptions
  }): Promise<Event> {
    // Get permissions
    const permissions: string[] = this.context.response.locals.permissions

    // Check if user has permission to create users
    if (!permissions || !permissions.includes('event:create')) {
      throw new Errors.ForbiddenError('You do not have permission to access this resource')
    }

    // Create Database user
    const event = Event.create({
      name: data.create.name,
      code: data.create.code,
      startDate: data.create.startDate,
      endDate: data.create.endDate,
      location: data.create.location,
      description: data.create.description
    })

    const vErrors = await validate(event)

    if (vErrors.length !== 0) {
      throw new Errors.BadRequestError(`The provided data is invalid: ${vErrors.map((verror) => verror.target).join(', ')}`)
    }

    return event.save()
  }
}
