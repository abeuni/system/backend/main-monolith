import { Path, GET, Param, Context, ServiceContext, Accept, PUT, OPTIONS, Errors } from 'typescript-rest'
import Member, { ICreateMemberOptions } from '@/models/members.model'
import { UpdateResult } from 'typeorm'
import { validate } from 'class-validator'

@Path('members?')
@Accept('application/json')
export class MembersResolver {
  @Context
  context: ServiceContext

  @OPTIONS
  options (): null {
    return null
  }

  @GET
  async listMembers (
    @Param('skip') skipStart: string,
    @Param('limit') limitStart: string
  ): Promise<Member[]> {
    const skip = parseInt(skipStart || '0')
    const limit = parseInt(limitStart || '50')

    return Member.find({ skip, take: limit })
  }

  @PUT
  async updateMember (
    data: {
      id: string,
      update: ICreateMemberOptions
    }
  ): Promise<UpdateResult> {
    // Get permissions
    const permissions: string[] = this.context.response.locals.permissions

    // Check if user has permission to create users
    if (!permissions || !permissions.includes('user:update')) {
      throw new Errors.ForbiddenError('You do not have permission to access this resource')
    }

    // Update member data in database
    const member = await Member.update({ id: data.id }, {
      ...data.update
    })

    const vErrors = await validate(member)

    if (vErrors.length !== 0) {
      throw new Errors.BadRequestError(`The provided data is invalid: ${vErrors.map((verror) => verror.target).join(', ')}`)
    }

    return member
  }
}
