import { Path, GET, Context, ServiceContext, Accept, OPTIONS, Errors, POST, PathParam } from 'typescript-rest'
import Member, { ICreateMemberOptions } from '@/models/members.model'
import { ResponseStatus } from '@/models/response.model.util'
import { DateTime } from 'luxon'
import { validate } from 'class-validator'
import { management } from '@/util/auth0'
import { v4 } from 'uuid'
import { logger } from '@/logging'

@Path('users?')
@Accept('application/json')
export class UsersResolver {
  @Context
  context: ServiceContext

  @OPTIONS
  options (): null {
    return null
  }

  @Path(':id?')
  @GET
  async user (
    @PathParam('id') id: string
  ): Promise<ResponseStatus<Member>> {
    // Get permissions
    const permissions: string[] = this.context.response.locals.permissions

    // TODO: Apply permissions for limiting sensitive data
    // If this user can read anybody's sensitive data (addresses, hidden fields)
    const canReadSensitive = permissions && permissions.includes('user:read')

    logger.info(canReadSensitive.toString())

    // Get current user if no id was given, or get another user if id was given
    let response = null

    if (id) response = await Member.findOne({ where: { id } })
    else response = await Member.findOne({ where: { accid: this.context.response.locals.user } })

    if (!response) throw new Errors.NotFoundError('Member was not found')

    return response
  }

  @POST
  async createUser (data: {
    create: ICreateMemberOptions
  }): Promise<Member> {
    // Get permissions
    const permissions: string[] = this.context.response.locals.permissions

    // Check if user has permission to create users
    if (!permissions || !permissions.includes('user:create')) {
      throw new Errors.ForbiddenError('You do not have permission to access this resource')
    }

    // Create Auth0 user
    const auth0Users = await management.getUsersByEmail(data.create.email)

    if (auth0Users.length < 1) {
      auth0Users.push(await management.createUser({
        email: data.create.email,
        password: v4(),
        connection: 'Username-Password-Authentication'
      }))

      await management.createPasswordChangeTicket({ user_id: auth0Users[0].user_id })
    }

    // Create Database user
    const member = Member.create({
      accid: auth0Users[0].user_id,
      fullname: data.create.fullname,
      email: data.create.email,
      RG: data.create.RG,
      CPF: data.create.CPF,
      birthDate: DateTime.fromISO(data.create.birthDate).toJSDate(),
      phone: data.create.phone,
      data: data.create.data
    })

    const vErrors = await validate(member)

    if (vErrors.length !== 0) {
      await management.deleteUser({ id: auth0Users[0].user_id })

      throw new Errors.BadRequestError(`The provided data is invalid: ${vErrors.map((verror) => verror.target).join(', ')}`)
    }

    return member.save()
  }
}
