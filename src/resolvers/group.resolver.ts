import { Path, Context, ServiceContext, Accept, OPTIONS, Errors, POST, GET, PathParam } from 'typescript-rest'
import Group, { ICreateGroupOptions } from '@/models/groups.model'
import { EntityNotFoundError } from 'typeorm/error/EntityNotFoundError'
import { validate } from 'class-validator'

@Path('groups?')
@Accept('application/json')
export class GroupsResolver {
  @Context
  context: ServiceContext

  @OPTIONS
  options (): null {
    return null
  }

  @Path('list')
  @GET
  async listGroups (): Promise<Group[]> {
    // Get permissions
    const permissions: string[] = this.context.response.locals.permissions

    // Check if user has permission to create users
    if (!permissions || !permissions.includes('group:list')) {
      throw new Errors.ForbiddenError('You do not have permission to access this resource')
    }

    return Group.find()
  }

  @Path(':id')
  @GET
  async getGroup (@PathParam('id') id: string): Promise<Group> {
    // Get permissions
    const permissions: string[] = this.context.response.locals.permissions

    // Check if user has permission to create users
    if (!permissions || !permissions.includes('group:read')) {
      throw new Errors.ForbiddenError('You do not have permission to access this resource')
    }

    return Group.findOneOrFail({ id }).catch((e) => {
      if (e instanceof EntityNotFoundError) { throw new Errors.NotFoundError(`Group with id ${id} not found`) }
      throw e
    })
  }

  @POST
  async createGroup (data: {
    create: ICreateGroupOptions
  }): Promise<Group> {
    // Get permissions
    const permissions: string[] = this.context.response.locals.permissions

    // Check if user has permission to create users
    if (!permissions || !permissions.includes('group:create')) {
      throw new Errors.ForbiddenError('You do not have permission to access this resource')
    }

    // Create Database user
    const group = Group.create({
      category: data.create.category,
      name: data.create.name,
      code: data.create.code,
      description: data.create.description
    })

    const vErrors = await validate(group)

    if (vErrors.length !== 0) {
      throw new Errors.BadRequestError(`The provided data is invalid: ${vErrors.map((verror) => verror.target).join(', ')}`)
    }

    return group.save()
  }
}
