import { createConnection, Connection } from 'typeorm'
import { logger } from '@/logging'
import { models } from '@/schema'

let connection: Connection = null

export const getConnection = async (): Promise<Connection> => {
  try {
    connection = await createConnection({
      type: 'postgres',
      url: process.env.PGSQL_URI || '',
      synchronize: true,
      logging: false,
      entities: models
    })

    connection.createEntityManager()

    logger.info('Connected to Database')
  } catch (e) {
    logger.error('Failed to connect', e)
  }

  return connection
}

getConnection()
