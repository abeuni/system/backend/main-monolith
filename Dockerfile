## USER CONFIG
# Yarn or npm ?
ARG package_manager=yarn
ARG package_lock=yarn.lock

# Create an environment variable for our default installation path
ARG project_root=/api

# Port
ARG port=3000

## Docker Magic from here on
# Our prodbase node images
FROM node:14.3-alpine AS prod

# Copy parameters to base image
ARG project_root

ENV INSTALL_PATH=${project_root}

# Set path as workdir
WORKDIR ${INSTALL_PATH}

# Our devbase node images
FROM node:14.3 AS base

# Copy parameters to base image
ARG package_manager
ARG package_lock
ARG project_root

ENV INSTALL_PATH=${project_root}
ENV LOCK_FILE=${package_lock}
ENV PACK_MANAGER=${package_manager}

# Set path as workdir
WORKDIR ${INSTALL_PATH}

###########################################################################
# Start development stage - basic node package installing and source copy #
###########################################################################
FROM base AS development

# We are in development
ENV NODE_ENV development

# Copy dependency files
COPY package.json ./
COPY ${LOCK_FILE} ./

# Install dependencies and devDependencies (NODE_ENV)
RUN ${PACK_MANAGER} install

# Copy remaining source code
COPY . .

# Exposes ports
EXPOSE ${port}

# At this point, may mount . as volume

###########################################################################
# Start build stage - babel build                                         #
###########################################################################
FROM development AS build

# Build src to pure javascript
RUN ${PACK_MANAGER} run build

# Remove current node_modules
RUN rm -rf node_modules

# Install producion-only dependencies (need .gyp and other libs)
RUN ${PACK_MANAGER} install --production

###########################################################################
# Production stage - setup for production                                 #
###########################################################################
FROM prod as production

# From here on we are in production
ENV NODE_ENV production

# Copy relevant files
COPY --from=build ${INSTALL_PATH}/dist/ ./dist
COPY --from=build ${INSTALL_PATH}/node_modules/ ./node_modules

# Exposes ports
EXPOSE ${port}

# Start in production
CMD node ./dist/server.js
